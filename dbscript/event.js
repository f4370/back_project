const mongoose = require('mongoose')
const Event = require('../models/event')
mongoose.connect('mongodb://localhost:27017/example')
async function clear () {
  await Event.deleteMany({})
}

async function main () {
  await clear()
  await Event.insertMany([
    {
      title: 'นาย ถิรวัฒน์ เจริญรื่น', content: 'IT301-01', startDate: new Date('2022-04-19 08:00'), endDate: new Date('2022-04-19 10:00'), class: 'b'
    },
    {
      title: 'นาย มงคล โชคอำนวยกิจ', content: 'IT301-02', startDate: new Date('2022-04-30 08:00'), endDate: new Date('2022-04-30 10:00'), class: 'a'
    },
    {
      title: 'นาย พานคำทอง โพธิสุวรรณ', content: 'IT301-03', startDate: new Date('2022-04-20 08:00'), endDate: new Date('2022-04-20 10:00'), class: 'c'
    },
    {
      title: 'นาย กิตติพศ เทพนา', content: 'IT301-04', startDate: new Date('2022-04-21 08:00'), endDate: new Date('2022-04-21 10:00'), class: 'b'
    },
    {
      title: 'นาย วงศธร รัตนนสาลี', content: 'IT301-05', startDate: new Date('2022-04-21 13:00'), endDate: new Date('2022-04-21 15:00'), class: 'c'
    }
  ])
  Event.find({})
}

main().then(function () {
  console.log('Finish')
})

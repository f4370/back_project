const express = require('express')
const router = express.Router()
const Building = require('../models/Building')
const getBuildings = async function (req, res, next) {
  try {
    const buildings = await Building.find({}).exec()
    res.status(200).json(buildings)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const getBuilding = async function (req, res, next) {
  const id = req.params.id
  console.log(id)
  try {
    const building = await Building.findById(id).exec()
    if (building === null) {
      return res.status(404).json({
        message: 'Building not found!!'
      })
    }
    res.json(building)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}
const addBuildings = async function (req, res, next) {
  const newBuilding = new Building({
    name: req.body.name,
    capacity: parseFloat(req.body.capacity)
  })
  try {
    await newBuilding.save()
    res.status(201).json(newBuilding)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateBuilding = async function (req, res, next) {
  const buildingId = req.params.id
  try {
    const building = await Building.findById(buildingId)
    building.name = req.body.name
    building.capacity = parseFloat(req.body.capacity)
    await building.save()
    return res.status(200).json(building)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}
const deleteBuilding = async function (req, res, next) {
  const buildingId = req.params.id
  try {
    await Building.findByIdAndDelete(buildingId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getBuildings)// GET Buildings
router.get('/:id', getBuilding)// GET One Building
router.post('/', addBuildings)// add New Buildings
router.put('/:id', updateBuilding)// add New Building
router.delete('/:id', deleteBuilding)// delete Building

module.exports = router

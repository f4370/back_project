const mongoose = require('mongoose')
const { Schema } = mongoose
const roomSchema = Schema({
  name: String,
  capacity: Number
})

module.exports = mongoose.model('Room', roomSchema)
